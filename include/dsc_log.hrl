%%% dsc_log.hrl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%

%% Define a traffic event
-type traffic_event() :: {
		TS :: pos_integer(), % posix time in milliseconds
		N :: pos_integer(), % unique number
		Node :: atom(),
		Service :: diameter:service_name(),
		OriginPeerHost :: undefined | string() | binary(),
		DestPeerHost :: undefined | string() | binary(),
		Request:: undefined | diameter_codec:message(),
		Answer :: undefined | diameter_codec:packet(),
		ResultCode :: undefined | integer()}.

