%%% dsc_api_SUITE.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  @doc Test suite for public API of the {@link //dsc. dsc} application.
%%%
-module(dsc_api_SUITE).
-copyright('Copyright (c) 2018 SigScale Global Inc.').

%% common_test required callbacks
-export([suite/0, sequences/0, all/0]).
-export([init_per_suite/1, end_per_suite/1]).
-export([init_per_testcase/2, end_per_testcase/2]).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include("dsc.hrl").
-include_lib("common_test/include/ct.hrl").
-include_lib("diameter/include/diameter.hrl").

%%---------------------------------------------------------------------
%%  Test server callback functions
%%---------------------------------------------------------------------

-spec suite() -> DefaultData :: [tuple()].
%% Require variables and set default values for the suite.
%%
suite() ->
	[{userdata, [{doc, "Test suite for public API in DSC"}]}].

-spec init_per_suite(Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before the whole suite.
%%
init_per_suite(Config) ->
	ok = dsc_test_lib:initialize_db(),
	ok = dsc_test_lib:start(),
	Config.

-spec end_per_suite(Config :: [tuple()]) -> any().
%% Cleanup after the whole suite.
%%
end_per_suite(Config) ->
	ok = dsc_test_lib:stop(),
	Config.

-spec init_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before each test case.
%%
init_per_testcase(_TestCase, Config) ->
	case is_alive() of
		true ->
			Config;
		false ->
			{skip, not_alive}
	end.

-spec end_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> any().
%% Cleanup after each test case.
%%
end_per_testcase(_TestCase, _Config) ->
	ok.

-spec sequences() -> Sequences :: [{SeqName :: atom(), Testcases :: [atom()]}].
%% Group test cases into a test sequence.
%%
sequences() -> 
	[].

-spec all() -> TestCases :: [Case :: atom()].
%% Returns a list of all test cases in this test suite.
%%
all() -> 
	[add_service, find_nonexisting_service, find_existing_service, delete_service,
		add_peer, find_nonexisting_peer, find_existing_peer, delete_peer, start_peer,
		add_realm, find_nonexisting_realm, find_existing_realm, delete_realm].

%%---------------------------------------------------------------------
%%  Test cases
%%---------------------------------------------------------------------

add_service() ->
	[{userdata, [{doc, "add a service to dsc_service table"}]}].

add_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, Svc} = dsc:add_service(Name, Address, Port, Options, Enabled),
	#dsc_service{name = Name, laddress = Address, lport = Port, enabled = Enabled,
			options = Options} = Svc.

find_nonexisting_service() ->
	[{userdata, [{doc, "Attempt to find non existing service in dsc_service table"}]}].

find_nonexisting_service(_Config) ->
	Name = rand:uniform(100000),
	{error, not_found} = dsc:find_service(Name).

find_existing_service() ->
	[{userdata, [{doc, "Attempt to find existing service in dsc_service table"}]}].

find_existing_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, _} = dsc:add_service(Name, Address, Port, Options, Enabled),
	{ok, Service} = dsc:find_service(Name),
	#dsc_service{name = Name, laddress = Address, lport = Port, enabled = Enabled,
			options = Options} = Service.

delete_service() ->
	[{userdata, [{doc, "Delete a service in dsc_service table"}]}].

delete_service(_Config) ->
	Name = rand:uniform(100000),
	Address = dsc_test_lib:ipv4(),
	Port = dsc_test_lib:port(),
	Enabled = true,
	Options = [],
	{ok, _} = dsc:add_service(Name, Address, Port, Options, Enabled),
	{ok, _} = dsc:find_service(Name),
	ok = dsc:delete_service(Name),
	{error, not_found} = dsc:find_service(Name).

add_peer() ->
	[{userdata, [{doc, "Add a peer to dsc_peer table"}]}].

add_peer(_Config) ->
	Name = dsc_test_lib:random_str(),
	Svc = dsc_test_lib:random_str(),
	LAddress = dsc_test_lib:ipv4(),
	LPort = dsc_test_lib:port(),
	RAddress = dsc_test_lib:ipv4(),
	RPort = dsc_test_lib:port(),
	Active = dsc_test_lib:peer_active(),
	{ok, Peer} = dsc:add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, [], Active),
	NameB = list_to_binary(Name),
	#dsc_peer{name = NameB, laddress = LAddress, lport = LPort,
			raddress = RAddress, rport = RPort, options = [], active = Active} = Peer.

find_nonexisting_peer() ->
	[{userdata, [{doc, "Find a non existing peer in dsc_peer table"}]}].

find_nonexisting_peer(_Config) ->
	Id = dsc_test_lib:random_str(),
	{error, not_found} = dsc:find_peer(Id).

find_existing_peer() ->
	[{userdata, [{doc, "Find an  existing peer in dsc_peer table"}]}].

find_existing_peer(_Config) ->
	Name = list_to_binary(dsc_test_lib:random_str()),
	Svc = dsc_test_lib:random_str(),
	LAddress = dsc_test_lib:ipv4(),
	LPort = dsc_test_lib:port(),
	RAddress = dsc_test_lib:ipv4(),
	RPort = dsc_test_lib:port(),
	Active = dsc_test_lib:peer_active(),
	{ok, _P} = dsc:add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, [], Active),
	{ok, Peer} = dsc:find_peer(Name),
	#dsc_peer{name= Name, active = Active, laddress = LAddress, lport = LPort,
			raddress = RAddress, rport = RPort, options = []} = Peer.

delete_peer() ->
	[{userdata, [{doc, "Delete a peer from dsc_peer table"}]}].

delete_peer(_Config) ->
	Name = dsc_test_lib:random_str(),
	Svc = dsc_test_lib:random_str(),
	LAddress = dsc_test_lib:ipv4(),
	LPort = dsc_test_lib:port(),
	RAddress = dsc_test_lib:ipv4(),
	RPort = dsc_test_lib:port(),
	Active = dsc_test_lib:peer_active(),
	{ok, _} = dsc:add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, [], Active),
	{ok, _} = dsc:find_peer(Name),
	ok = dsc:delete_peer(Name),
	{error, not_found} = dsc:find_peer(Name).

start_peer() ->
	[{userdata, [{doc, "Start a connection to a DIAMETER peer "}]}].

start_peer(_Config) ->
	%] Create listening DIAMETER peer
	Realm1 = "example.com",
	Host1 = "foo" ++ Realm1,
	AVPs1 = [{'Origin-Host', Host1},{'Origin-Realm', Realm1},
			{'Vendor-Id', 13019}, {'Product-Name', "Example DIAMETER listening peer"},
			{'Auth-Application-Id', [0]}],
	ServiceOptions1 = AVPs1 ++ [{application, [{dictionary, diameter_gen_base_rfc6733},
			{module, #diameter_callback{}}]}],
	Service1 = foo_example_service,
	true = diameter:subscribe(Service1),
	ok = diameter:start_service(Service1, ServiceOptions1),
	Address1 = {0,0,0,0},
	Port1 = 9563,
	Opts1 = [{transport_module, diameter_sctp},
			{transport_config, [{reuseaddr, true},
					{ip, Address1},
					{port, Port1}]}],
	TOptions1 = {listen, Opts1},
	{ok, _} = diameter:add_transport(Service1, TOptions1),
	receive
		#diameter_event{service = Service1, info = start} ->
			ok;
		_ ->
			ct:fail(service_not_started)
	end,
	receive after 2000 -> ok end,
	% Create a connecting peer
	Realm2 = "example.org",
	Host2 = "bar" ++ Realm2,
	AVPs2 = [{'Origin-Host', Host2},{'Origin-Realm', Realm2},
			{'Vendor-Id', 13019}, {'Product-Name', "Example DIAMETER connecting peer"},
			{'Auth-Application-Id', [0]}],
	ServiceOptions2 = AVPs2 ++ [{application, [{dictionary, diameter_gen_base_rfc6733},
			{module, #diameter_callback{}}]}],
	Service2 = bar_example_service,
	true = diameter:subscribe(Service2),
	ok = diameter:start_service(Service2, ServiceOptions2),
	receive
		#diameter_event{service = Service2, info = start} ->
			ok;
		_ ->
			ct:fail(service_not_started)
	end,
	Options2 = [{transport, sctp}],
	% Connect to listening peer
	{ok, _TRef} = dsc:start_peer(Service2, {0,0,0,0}, addr(Address1), Port1, Options2),
	receive
		#diameter_event{service = Service1, info = {up, _, _, _}} ->
			ok;
		#diameter_event{service = Service1, info = {up, _, _, _, _}} ->
			ok;
		_ ->
			ct:fail(capability_exchange_failed)
	end,
	[ConnectionInfo] = diameter:service_info(Service2, connections),
	{caps, Caps} = lists:keyfind(caps, 1, ConnectionInfo),
	{_, {Realm2, Realm1}} = lists:keyfind(origin_realm, 1, Caps),
	{_, {Host2, Host1}} = lists:keyfind(origin_host, 1, Caps),
	ok = diameter:stop_service(Service1),
	ok = diameter:stop_service(Service2).


add_realm() ->
	[{userdata, [{doc, "add a realm to dsc_realm table"}]}].

add_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, Realm} = dsc:add_realm(Name, AppId, Mode, Peers),
	NameB = list_to_binary(Name),
	#dsc_realm{name = NameB, app_id = AppId, mode = Mode, peers = Peers} = Realm.

find_nonexisting_realm() ->
	[{userdata, [{doc, "Find a non existing realm in dsc_realm table"}]}].

find_nonexisting_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	{error, not_found} = dsc:find_realm(Name).

find_existing_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

find_existing_realm(_Config) ->
	Name = list_to_binary(dsc_test_lib:random_str()),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, _} = dsc:add_realm(Name, AppId, Mode, Peers),
	{ok, Realm} = dsc:find_realm(Name),
	#dsc_realm{name = Name, app_id = AppId, mode = Mode, peers = Peers} = Realm.

delete_realm() ->
	[{userdata, [{doc, "Find an existing realm in dsc_realm table"}]}].

delete_realm(_Config) ->
	Name = dsc_test_lib:random_str(),
	AppId = rand:uniform(100),
	Mode = dsc_test_lib:realm_mode(),
	Peers = [dsc_test_lib:random_str()],
	{ok, _} = dsc:add_realm(Name, AppId, Mode, Peers),
	{ok, _} = dsc:find_realm(Name),
	ok = dsc:delete_realm(Name),
	{error, not_found} = dsc:find_realm(Name).

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

%% @hidden
addr({0,0,0,0}) ->
	{127,0,0,1};
addr(A) ->
	A.

