%%% dsc_log_SUITE.erl
%%% vim: ts=3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  @doc Test suite for logging functionality  of the {@link //dsc. dsc} application.
%%%
-module(dsc_log_SUITE).
-copyright('Copyright (c) 2018 SigScale Global Inc.').

%% common_test required callbacks
-export([suite/0, sequences/0, all/0]).
-export([init_per_suite/1, end_per_suite/1]).
-export([init_per_testcase/2, end_per_testcase/2]).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include("dsc.hrl").
-include_lib("common_test/include/ct.hrl").
-include_lib("diameter/include/diameter.hrl").
-include_lib("diameter/include/diameter_gen_base_rfc6733.hrl").

-define(MILLISECOND, milli_seconds).

%%---------------------------------------------------------------------
%%  Test server callback functions
%%---------------------------------------------------------------------

-spec suite() -> DefaultData :: [tuple()].
%% Require variables and set default values for the suite.
%%
suite() ->
	[{userdata, [{doc, "Test suite for public API in DSC"}]}].

-spec init_per_suite(Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before the whole suite.
%%
init_per_suite(Config) ->
	ok = dsc_test_lib:initialize_db(),
	ok = dsc_test_lib:start(),
	Config.

-spec end_per_suite(Config :: [tuple()]) -> any().
%% Cleanup after the whole suite.
%%
end_per_suite(Config) ->
	ok = dsc_test_lib:stop(),
	Config.

-spec init_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> Config :: [tuple()].
%% Initialization before each test case.
%%
init_per_testcase(_TestCase, Config) ->
	case is_alive() of
		true ->
			ok = dsc_log:traffic_open(),
			Config;
		false ->
			{skip, not_alive}
	end.

-spec end_per_testcase(TestCase :: atom(), Config :: [tuple()]) -> any().
%% Cleanup after each test case.
%%
end_per_testcase(_TestCase, _Config) ->
	ok = dsc_log:traffic_close().

-spec sequences() -> Sequences :: [{SeqName :: atom(), Testcases :: [atom()]}].
%% Group test cases into a test sequence.
%%
sequences() -> 
	[].

-spec all() -> TestCases :: [Case :: atom()].
%% Returns a list of all test cases in this test suite.
%%
all() -> 
	[traffic_log_event, traffic_query].

%%---------------------------------------------------------------------
%%  Test cases
%%---------------------------------------------------------------------

traffic_log_event() ->
   [{userdata, [{doc, "Log a DIAMETER traffice event"}]}].

traffic_log_event(_Config) ->
	Start = erlang:system_time(?MILLISECOND),
	Node = node(),
	Address = {0, 0, 0, 0},
	Port = 1812,
	Service = {Address, Port},
	DiamCaps = #diameter_caps{origin_host = dsc_test_lib:random_str(),
			origin_realm = dsc_test_lib:random_str(),
			vendor_id = 10415, product_name = "SigScale DSC"},
	OrigPeer = {orig_peer_ref, DiamCaps},
	DestPeer = {dest_peer_ref, DiamCaps},
	Request = #diameter_packet{},
	Answer = #diameter_packet{},
	ResultCode = 2001,
	ok = dsc_log:traffic_log(Service, OrigPeer, DestPeer, Request, Answer, ResultCode),
	End = erlang:system_time(?MILLISECOND),
	Fany = fun({TS, _, N, {A,P}, OP, DP, Req, Ans, ResCode}) when TS >= Start, TS =< End,
					N == Node, A == Address, P == Port, OP == OrigPeer, DP == DestPeer,
					Req  == Request, Ans == Answer, ResCode == ResultCode ->
				true;
			(_) ->
				false
	end,
	Find = fun(_F, {error, Reason}) ->
				ct:fail(Reason);
			(F, {Cont, Chunk}) ->
				case lists:any(Fany, Chunk) of
					false ->
						F(F, disk_log:chunk(dsc_traffic, Cont));
					true ->
						true
				end;
			(_F, eof) ->
				false
	end,
	true = Find(Find, disk_log:chunk(dsc_traffic, start)).

traffic_query() ->
   [{userdata, [{doc, "Query specific events from traffic log"}]}].

traffic_query(_Config) ->
	Service = {dsc_relay_service, {0,0,0,0}, 9784},
	OH = "ap1.sigscale.com",
	DH = "ap2.sigscale.org",
	UserName = list_to_binary(dsc_test_lib:random_str()),
	AcctAppId = 3,
	ResultCode = ?'DIAMETER_BASE_RESULT-CODE_SUCCESS',
	Header = #diameter_header{version = 1,cmd_code = 271},
	Avps = [#diameter_avp{code = 485, data = rand:uniform(10)},
			#diameter_avp{code = 480, data = rand:uniform(4)},
			#diameter_avp{code = 1, data = UserName},
			#diameter_avp{code = 259, data = AcctAppId},
			#diameter_avp{code = 296, data = OH},
			#diameter_avp{code = 264, data = DH},
			#diameter_avp{code = 268, data = ResultCode},
			#diameter_avp{code = 263, data = dsc_test_lib:random_str()}],
	Request = #diameter_packet{header = Header, avps = Avps},
	Answer = undefined,
	ok = fill_traffic(1000),
	LogInfo = disk_log:info(dsc_traffic),
	{_, {FileSize, _NumFiles}} = lists:keyfind(size, 1, LogInfo),
	{_, CurItems} = lists:keyfind(no_current_items, 1, LogInfo),
	{_, CurBytes} = lists:keyfind(no_current_bytes, 1, LogInfo),
	EventSize = CurBytes div CurItems,
	NumItems = (FileSize div EventSize) * 5,
	Start = erlang:system_time(?MILLISECOND),
	ok = fill_traffic(NumItems),
	ok = dsc_log:traffic_log(Service, OH, DH, Request, Answer, ResultCode),
	ok = fill_traffic(rand:uniform(2000)),
	ok = dsc_log:traffic_log(Service, OH, DH, Request, Answer, ResultCode),
	ok = fill_traffic(rand:uniform(2000)),
	ok = dsc_log:traffic_log(Service, OH, DH, Request, Answer, ResultCode),
	ok = fill_traffic(rand:uniform(2000)),
	End = erlang:system_time(?MILLISECOND),
	MatchReq = [{1, {exact, UserName}},
			{259, {exact, AcctAppId}}],
	Fget = fun(_F, {eof, Events}, Acc) ->
				lists:flatten(lists:reverse([Events | Acc]));
			(F, {Cont, Events}, Acc) ->
				F(F, dsc_log:traffic_query(Cont, Start, End, OH, DH, ResultCode, MatchReq), [Events | Acc])
	end,
	Events = Fget(Fget, dsc_log:traffic_query(start, Start, End, OH, DH, ResultCode, MatchReq), []),
	3 = length(Events).

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

fill_traffic(0) ->
	ok;
fill_traffic(N) ->
	Service = {dsc_relay_service, {0,0,0,0}, 6538},
	OH = dsc_test_lib:random_str() ++ ".sigscale.net",
	DH = dsc_test_lib:random_str() ++ ".sigscale.lk",
	ResultCode = case rand:uniform(5) of
		1 -> undefined;
		2 -> ?'DIAMETER_BASE_RESULT-CODE_SUCCESS';
		3 -> ?'DIAMETER_BASE_RESULT-CODE_UNABLE_TO_DELIVER';
		4 -> ?'DIAMETER_BASE_RESULT-CODE_UNKNOWN_PEER';
		5 -> ?'DIAMETER_BASE_RESULT-CODE_REALM_NOT_SERVED'
	end,
	Answer = case ResultCode of
		?'DIAMETER_BASE_RESULT-CODE_SUCCESS' ->
			Header = #diameter_header{version = 1,cmd_code = 271},
			AnsAvps = [#diameter_avp{code = 485, data = rand:uniform(10)},
					#diameter_avp{code = 480, data = rand:uniform(4)},
					#diameter_avp{code = 296, data = "sigscale.lk"},
					#diameter_avp{code = 264, data = DH},
					#diameter_avp{code = 268, data = ResultCode},
					#diameter_avp{code = 263, data = dsc_test_lib:random_str()}],
			#diameter_packet{header = Header, avps = AnsAvps};
		_ ->
			undefined
	end,
	ReqAvps = [#diameter_avp{code = 263, data = dsc_test_lib:random_str()},
			#diameter_avp{code = 264, data = OH},
			#diameter_avp{code = 296, data = "sigscale.net"},
			#diameter_avp{code = 283, data = "sigscale.lk"},
			#diameter_avp{code = 480, data = rand:uniform(4)},
			#diameter_avp{code = 485, data = rand:uniform(10)}],
	Request = #diameter_packet{avps = ReqAvps},
	ok = dsc_log:traffic_log(Service, OH, DH, Request, Answer, ResultCode),
	fill_traffic(N - 1).

